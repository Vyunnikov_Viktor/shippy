build:
	docker build -t template_requirements .
compose-build:
	docker build -t template_requirements .
	docker-compose build
requirements-update:
	go get -u
run-client:
	docker-compose up consignment-cli
compose-restart:
	docker build -t template_requirements .
	docker-compose build
	docker-compose up -d