package main

import (
	"context"
	"fmt"
	"github.com/micro/go-micro"
	pb "gitlab.com/Vyunnikov_Viktor/shippy/user-service/proto/user"
)

const (
	defaultFilename = "consignment.json"
)

type ClientService struct {
	client pb.UserServiceClient
}

func (srv *ClientService) GetAll() {
	fmt.Println("...:::GetAll:::...")
	response, err := srv.client.GetAll(context.Background(), &pb.Request{})

	fmt.Println("response = ", response)
	fmt.Println("error = ", err)
}

func (srv *ClientService) AddUser(user *pb.User) {
	fmt.Println("...:::AddUser:::...")
	response, err := srv.client.Create(context.Background(), user)
	fmt.Println("response = ", response)
	fmt.Println("error = ", err)
}

func (srv *ClientService) Get(user *pb.User) {
	fmt.Println("...:::Get:::...")
	response, err := srv.client.Get(context.Background(), user)
	fmt.Println("response = ", response)
	fmt.Println("error = ", err)
}

func main() {
	service := micro.NewService(micro.Name("shippy.user.cli"))
	service.Init()

	client := pb.NewUserServiceClient("shippy.service.user", service.Client())
	srv := ClientService{client}
	srv.GetAll()

	srv.AddUser(&pb.User{
		Id:       "",
		Name:     "Name",
		Company:  "Company",
		Email:    "Email",
		Password: "Password",
	})

	srv.GetAll()

	srv.Get(&pb.User{
		Id: "bd4e78b5-c858-49a7-bd53-4b246bc550c1",
	})

	srv.Get(&pb.User{
		Id: "bd4e78b5-c858-3-4b246bc550c1",
	})
}
