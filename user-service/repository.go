package main

import (
	"github.com/jinzhu/gorm"
	bp "gitlab.com/Vyunnikov_Viktor/shippy/user-service/proto/user"
)

type UserRepository interface {
	GetAll() ([]*bp.User, error)
	Create(user *bp.User) error
	Get(id string) (*bp.User, error)
	GetByEmailAndPassword(user *bp.User) (*bp.User, error)
}

type UserRepositoryImpl struct {
	db *gorm.DB
}

func (repo *UserRepositoryImpl) GetAll() ([]*bp.User, error) {
	var users []*bp.User

	if err := repo.db.Find(&users).Error; err != nil {
		return nil, err
	}

	return users, nil
}

func (repo *UserRepositoryImpl) Get(id string) (*bp.User, error) {
	user := &bp.User{Id: id}
	if err := repo.db.First(&user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

func (repo *UserRepositoryImpl) GetByEmailAndPassword(user *bp.User) (*bp.User, error) {

	if err := repo.db.First(&user).Error; err != nil {
		return nil, err
	}

	return user, nil

}

func (repo *UserRepositoryImpl) Create(user *bp.User) error {
	if err := repo.db.Create(&user).Error; err != nil {
		return err
	}
	return nil
}
