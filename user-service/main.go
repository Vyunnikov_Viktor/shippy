package main

import (
	"github.com/micro/go-micro"
	pb "gitlab.com/Vyunnikov_Viktor/shippy/user-service/proto/user"
	"log"
)

func main() {
	db, err := CreateConnection()

	if err != nil {
		log.Fatal(err)
	}

	db.AutoMigrate(&pb.User{})

	repo := UserRepositoryImpl{db}

	srv := micro.NewService(micro.Name("shippy.service.user"))

	pb.RegisterUserServiceHandler(srv.Server(), &service{&repo})

	if err := srv.Run(); err != nil {
		log.Fatal(err)
	}
}
