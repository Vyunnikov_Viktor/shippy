package user

import (
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

func (m *User) BeforeCreate(scope *gorm.Scope) error {
	newId := uuid.New()
	//var id = uuid.NewV4()
	return scope.SetColumn("Id", newId.String())
}
