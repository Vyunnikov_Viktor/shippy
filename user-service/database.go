package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"os"
)

const (
	defaultPort     = "5431"
	defaultHost     = "0.0.0.0"
	defaultUser     = "postgres"
	defaultDBName   = "postgres"
	defaultPassword = "postgres"
)

func CreateConnection() (*gorm.DB, error) {
	var port, host, user, DBName, password string
	var present bool
	// Get database details from environment variables

	if port, present = os.LookupEnv("DB_PORT"); !present {
		port = defaultPort
	}

	if host, present = os.LookupEnv("DB_HOST"); !present {
		host = defaultHost
	}

	if user, present = os.LookupEnv("DB_USER"); !present {
		user = defaultUser
	}

	if DBName, present = os.LookupEnv("DB_NAME"); !present {
		DBName = defaultDBName
	}

	if password, present = os.LookupEnv("DB_PASSWORD"); !present {
		password = defaultPassword
	}

	return gorm.Open(
		"postgres",
		fmt.Sprintf(
			"port=%s host=%s user=%s dbname=%s sslmode=disable password=%s",
			port, host, user, DBName, password,
		),
	)
}
