FROM golang

RUN mkdir /app
WORKDIR /app

ADD go.mod go.mod
RUN go mod download

RUN mkdir -p /app
WORKDIR /app

COPY . .


