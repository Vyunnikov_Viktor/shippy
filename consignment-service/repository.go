package main

import (
	"context"
	"fmt"
	pb "gitlab.com/Vyunnikov_Viktor/shippy/consignment-service/proto/consignment"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"sync"
)

type repository interface {
	Create(*pb.Consignment) error
	GetAll() ([]*pb.Consignment, error)
}

type MongoRepository struct {
	collection *mongo.Collection
}

func (repo *MongoRepository) Create(consignment *pb.Consignment) error {
	val, err := repo.collection.InsertOne(context.Background(), consignment)
	fmt.Print(val)
	return err
}

func (repo *MongoRepository) GetAll() ([]*pb.Consignment, error) {

	cur, err := repo.collection.Find(context.Background(), bson.D{{}}, nil)
	if err != nil {
		return nil, err
	}
	var consignments []*pb.Consignment
	//:= make([]*pb.Consignment, 0)

	for cur.Next(context.Background()) {
		var consignment *pb.Consignment
		if err := cur.Decode(&consignment); err != nil {
			return nil, err
		}
		consignments = append(consignments, consignment)
	}
	return consignments, nil
}

type Repository struct {
	mu           sync.RWMutex
	consignments []*pb.Consignment
}

func (repo *Repository) Create(consignment *pb.Consignment) (*pb.Consignment, error) {
	repo.mu.Lock()
	updated := append(repo.consignments, consignment)
	repo.consignments = updated
	repo.mu.Unlock()
	return consignment, nil
}

func (repo *Repository) GetAll() []*pb.Consignment {
	return repo.consignments
}
