package main

import (
	"context"
	"fmt"
	"github.com/micro/go-micro"
	"log"
	"os"

	pb "gitlab.com/Vyunnikov_Viktor/shippy/consignment-service/proto/consignment"
	vesselProto "gitlab.com/Vyunnikov_Viktor/shippy/vessel-service/proto/vessel"
)

const defaultHost = "mongodb://0.0.0.0:27017"

func main() {
	srv := micro.NewService(
		micro.Name("shippy.service.consignment"),
	)

	srv.Init()

	uri := os.Getenv("DB_HOST")

	if uri == "" {
		uri = defaultHost
	}

	client, err := CreateClient(uri)

	if err != nil {
		log.Panic(err)
	}

	defer client.Disconnect(context.TODO())

	consignmentCollection := client.Database("snippy").Collection("consignments")

	repository := &MongoRepository{consignmentCollection}

	vesselClient := vesselProto.NewVesselServiceClient("shippy.service.vessel", srv.Client())

	h := &handler{repository, vesselClient}

	pb.RegisterShippingServiceHandler(srv.Server(), h)

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
