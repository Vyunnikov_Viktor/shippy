package main

import (
	"context"
	pb "gitlab.com/Vyunnikov_Viktor/shippy/consignment-service/proto/consignment"
	vesselProto "gitlab.com/Vyunnikov_Viktor/shippy/vessel-service/proto/vessel"
)

type handler struct {
	repo         repository
	vesselClient vesselProto.VesselServiceClient
}

func (s *handler) CreateConsignment(ctx context.Context, req *pb.Consignment, res *pb.Response) error {
	vessel, err := s.vesselClient.FindAvailable(context.Background(), &vesselProto.Specification{
		Capacity:  int32(len(req.Containers)),
		MaxWeight: req.Weight,
	})

	if err != nil {
		return err
	}

	req.VesselId = vessel.Vessel.Id

	if err = s.repo.Create(req); err != nil {
		return err
	}

	res.Created = true
	res.Consignment = req
	return nil
}

func (s *handler) GetConsignments(ctx context.Context, req *pb.GetRequest, res *pb.Response) error {
	consignments, err := s.repo.GetAll()
	if err != nil {
		return err
	}
	res.Consignments = consignments
	return nil
}
