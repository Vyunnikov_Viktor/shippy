package main

import (
	"fmt"
	"github.com/micro/go-micro"
	bp "gitlab.com/Vyunnikov_Viktor/shippy/vessel-service/proto/vessel"
)

func main() {
	vessels := []*bp.Vessel{
		{Id: "one", Name: "Name one", Capacity: 100, MaxWeight: 10000},
	}

	repo := VesselRepositoryImpl{vessels: vessels}

	srv := micro.NewService(
		micro.Name("shippy.service.vessel"))

	srv.Init()

	bp.RegisterVesselServiceHandler(srv.Server(), &service{repo})

	if err := srv.Run(); err != nil {
		fmt.Print(err)
	}
}
