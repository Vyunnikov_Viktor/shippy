package main

import (
	"context"
	"fmt"
	bp "gitlab.com/Vyunnikov_Viktor/shippy/vessel-service/proto/vessel"
)

type service struct {
	repo VesselRepositoryImpl
}

func (s *service) FindAvailable(ctx context.Context, req *bp.Specification, res *bp.Response) error {
	fmt.Printf("Specification MaxWeight = %v Capacity = %v", req.MaxWeight, req.Capacity)
	vessel, err := s.repo.FindAvailable(req)
	if err != nil {
		return err
	}

	res.Vessel = vessel
	return nil
}
