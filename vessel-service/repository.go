package main

import (
	"errors"
	bp "gitlab.com/Vyunnikov_Viktor/shippy/vessel-service/proto/vessel"
)

type VesselRepository interface {
	FindAvailable(*bp.Specification) (*bp.Vessel, error)
}

type VesselRepositoryImpl struct {
	vessels []*bp.Vessel
}

func (repo *VesselRepositoryImpl) FindAvailable(spec *bp.Specification) (*bp.Vessel, error) {
	for _, vessel := range repo.vessels {
		if spec.Capacity <= vessel.Capacity && spec.MaxWeight <= vessel.MaxWeight {
			return vessel, nil
		}
	}

	return nil, errors.New("no vessel found by that spec")
}
